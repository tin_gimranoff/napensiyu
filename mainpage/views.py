from django.shortcuts import render

from textpage.models import News, Article


def index(request):
    last_news = News.objects.order_by('-published_date').all()[:5]
    last_articles = Article.objects.order_by('-published_date').all()[:5]
    return render(request, 'index.html', {
        'last_news': last_news,
        'last_articles': last_articles,
    })
