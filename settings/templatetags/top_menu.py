from django import template

from settings.models import Menu
from textpage.models import Page

register = template.Library()


@register.inclusion_tag('_top_menu.html')
def top_menu():
    articles_submenu = Menu.objects.filter(parent='articles').order_by('name').all()
    news_submenu = Menu.objects.filter(parent='news').order_by('name').all()
    pages = Page.objects.filter(show_on_menu=True).order_by('name').all()
    return {
        'articles_submenu': articles_submenu,
        'news_submenu': news_submenu,
        'pages': pages,
    }
