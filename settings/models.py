from django.db import models
from django.template.defaultfilters import slugify
from transliterate import translit


class SlugModel(models.Model):
    class Meta:
        abstract = True

    alias = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Алиас для URL', help_text=u'Если оставить пустым заполнится автоматически')
    description = models.TextField(verbose_name=u'Meta Description', null=True, blank=True, help_text=u'Если оставить пустым заполнится автоматически')

    def save(self, add_id_to_end_url=False, *args, **kwargs):
        if self.alias is not None and self.alias != '':
            alias_translit = slugify(translit(str(self.alias), 'ru', reversed=True))
            if self.alias != alias_translit:
                self.alias = alias_translit
        super().save(*args, **kwargs)
        if self.alias is None or self.alias == '':
            self.alias = slugify(translit(str(self.name), 'ru', reversed=True))
            if add_id_to_end_url:
                self.alias = self.alias+'_' + str(self.pk)
            self.save()


class Menu(models.Model):

    PARENTS_ELEMENTS = (
        ('articles', 'Статьи'),
        ('news', 'Новости'),
    )

    class Meta:
        db_table = 'menus'
        verbose_name = u'Меню'
        verbose_name_plural = u'Меню'

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'Название меню')
    alias = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'Ссылка/Алиас')
    parent = models.CharField(max_length=255, choices=PARENTS_ELEMENTS, default='articles', verbose_name=u'Родительская страница')
