from django.contrib import admin
from settings.models import Menu


class MenuAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'alias')


admin.site.register(Menu, MenuAdmin)
