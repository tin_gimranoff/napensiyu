from django.shortcuts import render


def error_404_view(request, exception):
    response = render(request, 'textpage.html', {
        'title': 'Ошибка 404',
        'object': {
            'name': 'Ошибка 404',
            'body': 'Такой страницы не существует',
        }
    })
    response.status_code = 404
    return response
