"""napensiyu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include, re_path

from napensiyu import settings

from mainpage import views as mainpage_view
from textpage import views as textpage_view
from calculator import views as calculator_view

urlpatterns = [
    path('jet/', include('grappelli.urls')),  # grappelli URLSs
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', mainpage_view.index),
    path('calculator/', calculator_view.calculate),
    re_path('^(?P<type>(articles|news))/$', textpage_view.page),
    re_path('^(?P<type>(articles|news))/(?P<param_level_1>[\w-]+)/$', textpage_view.page),
    re_path('^(?P<type>(articles|news))/(?P<param_level_1>[\w-]+)/(?P<param_level_2>[\w-]+)/$', textpage_view.page),
    path('<str:alias>/', textpage_view.show_textpage),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler404 = 'errors.views.error_404_view'
