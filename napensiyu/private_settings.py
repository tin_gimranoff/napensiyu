# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = True

SESSION_DOMAIN = '.local.napensiyu.ru'
ALLOWED_HOSTS = ['local.napensiyu.ru',]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'napensiyu',
        'USER': 'postgres',
        'PASSWORD': 'koi8rus',
        'HOST': 'localhost',
        'PORT': '5432',
        # 'OPTIONS': {'charset': 'utf8'},
    }
}