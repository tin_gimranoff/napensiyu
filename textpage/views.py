from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from settings.models import Menu
from textpage.models import News, Article, Page


def page(request, type, param_level_1=None, param_level_2=None):
    template = None
    title = []
    if type == 'news':
        title.append('Новости')
    else:
        title.append('Статьи')
    if param_level_1 is None:
        # Нужно вытащить все объекты
        if type == 'news':
            objects = News.objects.order_by('-published_date').all()
        else:
            objects = Article.objects.order_by('-published_date').all()
        template = 'category.html'

    if param_level_1 is not None and param_level_2 is None:
        if type == 'news':
            object = News.objects.filter(alias=param_level_1).first()
        if type == 'articles':
            object = Article.objects.filter(alias=param_level_1).first()
        if object:
            title.append(object.name)
            template = 'content.html'
        else:
            subcategory = Menu.objects.get(alias=param_level_1)
            if subcategory is not None:
                title.append(subcategory.name)
                # Нужно вытащить все объекты
                if type == 'news':
                    objects = News.objects.filter(subcategory=subcategory).order_by('-published_date').all()
                else:
                    objects = Article.objects.filter(subcategory=subcategory).order_by('-published_date').all()
                template = 'category.html'

    if param_level_2 is not None:
        if type == 'news':
            object = News.objects.get(alias=param_level_2)
        if type == 'articles':
            object = Article.objects.get(alias=param_level_2)
        if object:
            title.append(object.subcategory.name)
            title.append(object.name)
            template = 'content.html'

    if template is None:
        return HttpResponse(status=404)

    title = " — ".join(reversed(title))
    if template == 'category.html':
        return render(request, template, {
            'title': title,
            'objects': objects,
            'type': type
        })
    if template == 'content.html':
        return render(request, template, {
            'title': title,
            'object': object,
        })


def show_textpage(request, alias):
    page = get_object_or_404(Page, alias=alias)
    return render(request, 'textpage.html', {
        'title': page.name,
        'object': page,
        'meta_description': page.description
    })
