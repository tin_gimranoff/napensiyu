from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from settings.models import SlugModel, Menu
from easy_thumbnails.fields import ThumbnailerImageField


class News(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'news'
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    published_date = models.DateTimeField(verbose_name=u'Дата публикации', blank=False, null=False)
    name = models.CharField(max_length=255, verbose_name=u'Заголовок новости', blank=False, null=False)
    intro = models.CharField(max_length=255, verbose_name=u'Краткое содержание для главной', blank=True, null=True)
    body = RichTextUploadingField(verbose_name=u'Тело новости')
    news_cover = ThumbnailerImageField(upload_to='news/', blank=False, null=False, verbose_name=u'Обложка')
    subcategory = models.ForeignKey(Menu, limit_choices_to={'parent': 'news'}, verbose_name=u'Подкатегория если есть', on_delete=models.SET_NULL, blank=True, null=True)


class Article(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'articles'
        verbose_name = u'Статья'
        verbose_name_plural = u'Статьи'

    published_date = models.DateTimeField(verbose_name=u'Дата публикации', blank=False, null=False)
    name = models.CharField(max_length=255, verbose_name=u'Заголовок новости', blank=False, null=False)
    intro = models.CharField(max_length=255, verbose_name=u'Краткое содержание для главной', blank=True, null=True)
    body = RichTextUploadingField(verbose_name=u'Тело новости')
    news_cover = ThumbnailerImageField(upload_to='news/', blank=False, null=False, verbose_name=u'Обложка')
    subcategory = models.ForeignKey(Menu, limit_choices_to={'parent': 'articles'}, verbose_name=u'Подкатегория если есть', on_delete=models.SET_NULL, blank=True, null=True)


class Page(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'pages'
        verbose_name = u'Страницы'
        verbose_name_plural = u'Страницы'

    name = models.CharField(max_length=255, verbose_name=u'Название страницы', blank=False, null=False)
    body = RichTextUploadingField(verbose_name=u'Содержимое')
    show_on_menu = models.BooleanField(verbose_name=u'Показывать в меню', default=False)
