from django.apps import AppConfig


class TextpageConfig(AppConfig):
    name = 'textpage'
    verbose_name = u'Текстовые страницы'
