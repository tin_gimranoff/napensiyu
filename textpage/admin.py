from django.contrib import admin

from textpage.models import *


admin.site.register(News)
admin.site.register(Article)
admin.site.register(Page)
