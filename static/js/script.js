$(document).ready(function(){

	$(document).click(function(e){
		//e.preventDefault()
		var container = $(".mobile-calculator-form-popup");
	    if (container.has(e.target).length === 0){
	        container.hide();
	    }
	});

	$('.input-date').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',
    });
    resize();
    $(window).resize(function() {
    	resize();
    });
    $(".calculator-toggler").click(function(event){
    	$('.mobile-calculator-form-popup').toggle(300);
    	event.stopPropagation();
    });
});

function resize() {
	// Переключатели Новости/Статьи
	if (window.innerWidth < 767) {
		$($("section .page-header")[1]).addClass('inactive');
		$($("section .section-content")[1]).css('display', 'none');
		$("main").on('click', ".page-header.inactive", function() {
			var inactive_section_index = $(this).parent().index();
			console.log(inactive_section_index);
			if(inactive_section_index == 1) 
				var active_section_index = 0;
			else
				var active_section_index = 1;
			$($("section .page-header")[active_section_index]).addClass('inactive');
			$($("section .section-content")[active_section_index]).css('display', 'none');
			$($("section .page-header")[inactive_section_index]).removeClass('inactive');
			$($("section .section-content")[inactive_section_index]).css('display', 'block');

		});
	} else {
		$($("section .page-header")).removeClass('inactive');
		$($("section .section-content")).css('display', 'block');
	}

}