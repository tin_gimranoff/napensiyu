from django.db import models


class Country(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'countries'
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'
    name = models.CharField(verbose_name=u'Страна', blank=False, null=False, max_length=255)
    name_for_text = models.CharField(verbose_name=u'Страна в местном падеже', blank=False, null=False, max_length=255)


class Calculator(models.Model):

    def __str__(self):
        return ('%s (от %s до %s)') % (self.country.name, self.date_birth_start, self.date_birth_end if self.date_birth_end else '-')

    class Meta:
        db_table = 'low_redactions'
        verbose_name = 'Редакция закона'
        verbose_name_plural = 'Редакции закона'

    country = models.ForeignKey(Country, null=False, blank=False, verbose_name=u'', on_delete=models.CASCADE)
    date_birth_start = models.DateField(null=False, blank=False, verbose_name=u'Дата начала')
    date_birth_end = models.DateField(null=True, blank=True, verbose_name=u'Дата окончания', help_text='Оставить пустым если по настоящее время')
    years_male = models.IntegerField(verbose_name=u'Количество лет', default=0)
    months_male = models.IntegerField(verbose_name=u'Количество месяцев', default=0)
    years_female = models.IntegerField(verbose_name=u'Количество лет', default=0)
    months_female = models.IntegerField(verbose_name=u'Количество месяцев', default=0)
    low = models.TextField(verbose_name=u'', blank=True, null=True)
