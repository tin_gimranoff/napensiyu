from django.contrib import admin
from calculator.models import *

class CalculatorAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Страна', {
            'fields': ('country',),
        }),
        ('Промежуток дат действия закона', {
            'fields': ('date_birth_start', 'date_birth_end'),
        }),
        ('Возраст мужчин', {
            'fields': ('years_male', 'months_male'),
        }),
        ('Возраст женщин', {
            'fields': ('years_female', 'months_female'),
        }),
        ('Выдержка из закона', {
            'fields': ('low',),
        }),
    )

admin.site.register(Country)
admin.site.register(Calculator, CalculatorAdmin)