from calculator.forms import HeaderCalculatorForm, PopupCalculatorForm


def template_form_processor(request):
    if request.POST:
        header_template_form = HeaderCalculatorForm(request.POST)
        popup_template_form = PopupCalculatorForm(request.POST)
        if not header_template_form.is_valid():
            for key in header_template_form.errors.as_data():
                header_template_form.fields[key].widget.attrs['class'] += ' is-invalid'
        if not popup_template_form.is_valid():
            for key in popup_template_form.errors.as_data():
                popup_template_form.fields[key].widget.attrs['class'] += ' is-invalid'
    else:
        header_template_form = HeaderCalculatorForm
        popup_template_form = PopupCalculatorForm
    return {
        'header_template_form': header_template_form,
        'popup_template_form': popup_template_form,
    }
