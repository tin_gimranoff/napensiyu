import datetime
import locale

from django.db.models import Q
from django.shortcuts import render
from calculator.models import Calculator

from calculator.forms import BodyCalculatorForm


def calculate(request):
    locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    context = {}
    if request.GET and request.GET.get('birthday_date', None) is not None:
        form = BodyCalculatorForm(request.GET)
        if form.is_valid():
            cleared_data = form.clean()
            calculated = datetime.datetime.strptime(cleared_data['birthday_date'], '%d.%m.%Y')
            country = Calculator.objects.filter(country__id=cleared_data['country']).\
                filter(Q(date_birth_start__lte=calculated, date_birth_end__gte=calculated) | Q(date_birth_start__lte=calculated, date_birth_end__isnull=True)).first()
            if country:
                if cleared_data['gender'] == 'male':
                    if calculated.month + country.months_male > 11:
                        _years = country.years_male + 1
                        _months = country.months_male - 12
                    else:
                        _years = country.years_male
                        _months = country.months_male
                else:
                    if calculated.month + country.months_female > 11:
                        _years = country.years_female + 1
                        _months = country.months_female - 12
                    else:
                        _years = country.years_female
                        _months = country.months_female
                _new_date = datetime.datetime(calculated.year + _years, calculated.month + _months, calculated.day).strftime('%d %B %Y')
                _age = str(_years) + form.year_str(_years)
                if _months > 0:
                    _age = _age + u' и ' + str(_months) + form.month_str(_months)
                context = {
                    'h1': 'В каком возрасте в %s выходят на пенсию %s, рожденные %s года' % (country.country.name_for_text, 'мужчины' if cleared_data['gender'] == 'male' else 'женщины', calculated.strftime('%d %B %Y')),
                    'low': country.low,
                    'country_for_text': country.country.name_for_text,
                    'new_date': _new_date,
                    'gender': 'Мужчина' if cleared_data['gender'] == 'male' else 'Женщина',
                    'birthday': calculated.strftime('%d %B %Y'),
                    'age': _age,
                    'title': 'Когда выходит на пенсию %s %s года рождения в %s' % ('мужчина' if cleared_data['gender'] == 'male' else 'женщина', calculated.strftime('%d %B %Y'), country.country.name_for_text),
                    'description': 'В каком году и во сколько лет пойдет на пенсию %s %s года рождения в %s. Расчет точной даты.' % ('мужчина' if cleared_data['gender'] == 'male' else 'женщина', calculated.strftime('%d %B %Y'), country.country.name_for_text)
                }
            else:
                form.add_error('birthday_date', 'В системе нет информации для данной даты.')
                for key in form.errors.as_data():
                    form.fields[key].widget.attrs['class'] += ' is-invalid'
        else:
            for key in form.errors.as_data():
                form.fields[key].widget.attrs['class'] += ' is-invalid'
    else:
        form = BodyCalculatorForm
    if 'title' not in context.keys():
        context['title'] = 'Рассчет'
    context['form'] = form
    return render(request, 'calculate.html', context)
