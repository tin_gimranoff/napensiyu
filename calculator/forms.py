import datetime
import random

from django import forms

from calculator.models import Country

GENDER_CHOISES = (
    ('male', 'М'),
    ('female', 'Ж'),
)


class HeaderCalculatorForm(forms.Form):
    birthday_date = forms.CharField(
        widget=forms.TextInput(attrs={
            'id': 'datetime-peacker',
            'class': 'form-control input-date',
            'autocomplete': 'off',
        }), required=True
    )

    gender = forms.ChoiceField(choices=GENDER_CHOISES, widget=forms.RadioSelect(attrs={
        'id': 'header_gender',
        'class': 'custom-control-input',
        'group': 'header_form',
    }), required=True, initial='male')

    country = forms.CharField(
        widget=forms.Select(choices=Country.objects.values_list('id', 'name').order_by('name').all(), attrs={
            'class': 'form-control',
            'id': 'country',
        })
    )

    @staticmethod
    def year_str(year):
        year = str(year)
        if len(year) == 1:
            if int(year) == 1:
                return u' год'
            if 1 < int(year) < 5:
                return u' года'
            if 4 < int(year) < 21 or int(year) == 0:
                return u' лет'
        if len(year) > 1:
            buff = int(year[-2:])
            if 0 <= buff <= 20:
                return u' лет'
            elif int(str(buff)[1]) == 1:
                return u' год'
            elif 1 < int(str(buff)[1]) < 5:
                return u' года'
            elif int(str(buff)[1]) > 4:
                return u' лет'

    @staticmethod
    def month_str(months):
        if int(months) == 1:
            return u' месяц'
        elif 1 < int(months) < 5:
            return u' месяца'
        elif 5 <= int(months) <= 12:
            return u' месяцев'


class PopupCalculatorForm(HeaderCalculatorForm):
    gender = forms.ChoiceField(choices=GENDER_CHOISES, widget=forms.RadioSelect(attrs={
        'id': 'popup_gender',
        'class': 'custom-control-input',
        'group': 'popup_form',
    }), required=True, initial='male')


class BodyCalculatorForm(HeaderCalculatorForm):
    birthday_date = forms.CharField(
        widget=forms.TextInput(attrs={
            'id': 'datetime-peacker-page',
            'class': 'form-control input-date',
            'autocomplete': 'off',
        }), required=True
    )

    gender = forms.ChoiceField(choices=GENDER_CHOISES, widget=forms.RadioSelect(attrs={
        'id': 'body_gender',
        'class': 'custom-control-input',
        'group': 'body_form',
    }), required=True, initial='male')
